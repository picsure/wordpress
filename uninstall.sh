#!/bin/bash
BASEDIR=$(pwd)
CRONTABLINE="0 12 * * * $BASEDIR/ssl_renew.sh >> /var/log/cron.log 2>&1"
crontab -u $(whoami) -l | grep -v "ssl_renew"  | crontab -u $(whoami) -
docker-compose down -v --rmi all --remove-orphans
